const MIN_CONFIDENCE = 70;
const filterUnconfidentChunks = (chunks = []) =>
  chunks.filter(({ confidence }) => confidence >= MIN_CONFIDENCE);

module.exports = {
  filterUnconfidentChunks,
};
