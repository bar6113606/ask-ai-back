const express = require("express");
const { inferenceAskQuestion } = require("../services/inference");
const { generateToken, getChunkData } = require("../services/chunk-holder");
const { filterUnconfidentChunks } = require("../utils/question");
const router = express.Router();
router.post("/ask", async function (req, res, next) {
  try {
    const { question } = req.body;
    const { chunks } = await inferenceAskQuestion(question);
    const { token } = await generateToken();
    const filteredChunks = filterUnconfidentChunks(chunks);

    const promises = filteredChunks.map(({ chunkId }) =>
      getChunkData({ token, chunkId })
    );
    const result = await Promise.all(promises);

    res.send({ data: result });
  } catch (e) {
    res.send(e);
  }
});

module.exports = router;
