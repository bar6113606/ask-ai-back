const axios = require("axios");

const URL = "https://chunk-holder.hw.ask-ai.co";
async function generateToken() {
  try {
    const result = await axios({
      method: "post",
      url: `${URL}/auth/generate-token`,
      headers: {
        "x-api-key": "d486a94c-29f4-453a-a822-f909a97dbfa7",
        "Content-Type": "application/json",
      },
    });

    return result.data;
  } catch (e) {
    throw e;
  }
}

async function getChunkData({ token, chunkId }) {
  try {
    const result = await axios({
      method: "get",
      url: `${URL}/chunks/${chunkId}`,
      headers: {
        Authorization: token,
        "Content-Type": "application/json",
      },
    });

    return result.data;
  } catch (e) {
    throw e;
  }
}

module.exports = {
  generateToken,
  getChunkData,
};
